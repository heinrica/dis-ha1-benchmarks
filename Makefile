CC := gcc
FLAGS := -Wall -Wextra -Wconversion 
BIN := forksum

all:
	$(CC) $(FLAGS) -o $(BIN) forksum.c

clean:
	rm $(BIN)
